/**
 * @file
 * IEF attachments for auto-add functionality.
 */

 (function ($, Drupal, drupalSettings) {
  "use strict";

  /**
   * Current Event
   */
  var EVENT = null;

  /**
   * Event for add existing entity button.
   * @type {Event}
   */
  var ADD_EXISTING_BUTTON_CLICKED_EVENT = new Event(
    "add-exiting-button-clicked"
  );

  /**
   * Event for item select action.
   * @type {Event}
   */
  var ADD_NEW_ITEM_LIST_ELEMENT_CLICKED = new Event(
    "add-new-item-list-element-clicked"
  );

  /**
   * Flag to indicate if the user selected the "add new" list item.
   * @type {boolean}
   */
  var NEW_ITEM_SELECTED = false;

  /**
   * The auto complete field, populated by attach function.
   * @type {Element}
   */
  var AUTO_COMPLETE_FIELD = null;

  /**
   * Overridden from core/misc/autocomplete.js to select new item.
   */
  function selectHandler(event, ui) {
    // Check for our new item.
    if (ui.item.value === "erw-ief-item") {
      $(getAddingExistingCancelButton())
        .mousedown();
      EVENT = ADD_NEW_ITEM_LIST_ELEMENT_CLICKED;
    } // Otherwise just use core's implementation.
    else {
      Drupal.autocomplete.options.__select(event, ui);
    }

    return false;
  }

  /**
   * Attaches the autocomplete behavior to all required fields.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the autocomplete behaviors.
   * @prop {Drupal~behaviorDetach} detach
   *   Detaches the autocomplete behaviors.
   */
  Drupal.behaviors.ErwIef = {
    attach: function attach(context, settings) {
      // Adds a 'Create new item' option to the autocomplete.
      var $autocomplete = $(once('erw-autocomplete', 'input.form-autocomplete', context));

      if ($autocomplete.length) {
        var createText = drupalSettings.entity_reference_widgets.erw_new_item_text;
        $autocomplete
          .autocomplete(Drupal.autocomplete.options)
          .each(function () {
            var $this = $(this);

            if ($this.closest(".erw-ief-element")
              .length) {
              AUTO_COMPLETE_FIELD = $this;
              // Add the option if not already..
              $this.on("autocompleteresponse", function (event, ui) {
                var added = $.grep(ui.content, function (e) {
                  return e.value === "erw-ief-item";
                });

                if (!added.length) {
                  ui.content.push({
                    value: "erw-ief-item",
                    label: createText
                  });
                  ui.content.reverse();
                }
              });
            }
          });
      }

      //Always rebind events to the new elements that show up.
      bindEventsToElements();
    }
  };

  /**
   * Return the field name that we're working with.
   * @returns {string|*}
   */
  var getFieldName = function getFieldName() {
    return drupalSettings.entity_reference_widgets.field_name;
  };

  /**
   * Return the widget container element.
   * @returns {Element}
   */
  var getContainerElement = function getContainerElement() {
    return document.querySelector(
      "#inline-entity-form-".concat(getFieldName(), "-form")
    );
  };

  /**
   * Return the add new entity button.
   * @returns {Element}
   */

  var getAddNewButton = function getAddNewButton() {
    return document.querySelector("[data-erw-ief-add-name]");
  };

  /**
   * Return the add existing entity button.
   * @returns {Element}
   */
  var getAddExistingButton = function getAddExistingButton() {
    return document.querySelector(
      '[name="ief-'.concat(getFieldName(), '-form-add-existing"]')
    );
  };

  /**
   * Return the edit entity button for new entity.
   * @returns {Element}
   */
  var getEditNewButton = function getEditNewButton() {
    return document.querySelector(
      '[name*="ief-edit-submit-'.concat(getFieldName(), '-form"]')
    );
  };

  /**
   * Return the cancel button when adding existing entity.
   * @returns {Element}
   */
  var getAddingExistingCancelButton = function getAddingExistingCancelButton() {
    return document.querySelector(
      '[name="ief-reference-cancel-'.concat(getFieldName(), '-form"]')
    );
  };

  /**
   * Return the submit button when adding a existing entity.
   * @returns {Element}
   */
  var getAddingExistingSaveButton = function getAddingExistingSaveButton() {
    return document.querySelector(
      '[name="ief-reference-submit-'.concat(getFieldName(), '-form"]')
    );
  };

  /**
   * Return the submit button when adding a new entity entity.
   * @returns {Element}
   */
  var getSubmitButton = function getSubmitButton() {
    return document.querySelector(
      '[name="ief-add-submit-'.concat(getFieldName(), '-form"]')
    );
  };

  var isRequired = function isRequired() {
    var container = getContainerElement().querySelector(
      '.erw-ief-element'
    );
    if (!container) {
      return false;
    }
    var required = container.querySelector(
      'legend .form-required'
    );

    return required !== null;
  }

   var hasExistingSelection = function hasSelection() {
     var existing = getContainerElement().querySelector(
       '.erw-ief-element .ief-row-entity'
     );

     return existing !== null;
   }

  /**
   * Bind the events in the buttons so they can show/hide/clicked.
   */
  var bindEventsToElements = function bindEventsToElements() {

    // Cancel button when adding existing entity.
    if (getAddingExistingCancelButton()) {
      getAddingExistingCancelButton()
        .addEventListener(
          ADD_EXISTING_BUTTON_CLICKED_EVENT.type,
          function (event) {
            $(event.target)
              .parent()
              .parent()
              .parent()
              .find("legend")
              .html(drupalSettings.entity_reference_widgets.erw_helper_text);
            if (!hasExistingSelection() && isRequired()) {
              $(event.target)
                .hide();
            }
          }
        );

      if (EVENT && EVENT === ADD_EXISTING_BUTTON_CLICKED_EVENT) {
        getAddingExistingCancelButton()
          .dispatchEvent(EVENT);
      }
    }

    // Save button when adding existing entity.
    if (getAddingExistingSaveButton()) {
      $(getAddingExistingSaveButton())
        .hide();
    }

    // Update button when updating a recent added entity.
    if (getEditNewButton()) {
      $(getEditNewButton()).prop('value', Drupal.t('Update'));
    }

    // Submit button when adding existing entity.
    if (getSubmitButton()) {
      $(getSubmitButton())
        .prop('value', drupalSettings.entity_reference_widgets.erw_save_text);
      getSubmitButton()
        .addEventListener(
          ADD_EXISTING_BUTTON_CLICKED_EVENT.type,
          function (event) {
            $(event.target)
              .hide();
          }
        );

      if (EVENT && EVENT === ADD_EXISTING_BUTTON_CLICKED_EVENT) {
        getSubmitButton()
          .dispatchEvent(EVENT);
      }
    }


    // Add new list option selected.
    if (getAddNewButton()) {
      getAddNewButton()
        .addEventListener(
          ADD_NEW_ITEM_LIST_ELEMENT_CLICKED.type,
          function (event) {
            $(getAddExistingButton())
              .hide();
            $(event.target)
              .hide();
            $(event.target)
              .mousedown();
          }
        );

      if (EVENT && EVENT === ADD_NEW_ITEM_LIST_ELEMENT_CLICKED) {
        getAddNewButton()
          .dispatchEvent(EVENT);
        NEW_ITEM_SELECTED = true;
        EVENT = null;
      }
    }

    // If the new selected item is true and the add existing button is visible
    // the user clicked in the canceled button when inserting new entity, so, restart the flow.
    if (getAddExistingButton() && $(getAddExistingButton()).is(":visible")) {
      NEW_ITEM_SELECTED = false;
      if (hasExistingSelection()) {
        $(getAddNewButton()).hide();
        return;
      }
      $(getAddExistingButton())
        .parent()
        .hide();
      $(getAddExistingButton())
        .mousedown();
      EVENT = ADD_EXISTING_BUTTON_CLICKED_EVENT;
    }
  };

  /**
   * Init method when the page loads.
   */
  var init = function init() {
    jQuery("document")
      .ready(function () {
        // Case when form was submitted with content that does not match any item in the autocomplete.
        if ($(AUTO_COMPLETE_FIELD).hasClass('error')) {
          $(AUTO_COMPLETE_FIELD)
            .parent()
            .parent()
            .parent()
            .find("legend")
            .html(drupalSettings.entity_reference_widgets.erw_helper_text);
          $(AUTO_COMPLETE_FIELD)
            .parent()
            .parent()
            .find('[name="ief-reference-cancel-'.concat(getFieldName(), '-form"]')).hide();
          return;
        }
        $(getAddNewButton()).hide();
        if (hasExistingSelection()) {
          return;
        }
        $(getAddExistingButton())
          .css("visibility", "hidden");
        $(getAddExistingButton())
          .mousedown();
        EVENT = ADD_EXISTING_BUTTON_CLICKED_EVENT;
      });
  };

  // Override the select handler from core.
  Drupal.autocomplete.options.__select = Drupal.autocomplete.options.select;
  Drupal.autocomplete.options.select = selectHandler;
  init();
})(jQuery, Drupal, drupalSettings);
