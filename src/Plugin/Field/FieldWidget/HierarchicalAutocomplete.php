<?php

namespace Drupal\entity_reference_widgets\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of an entity reference with parent selection.
 *
 * @FieldWidget(
 *   id = "entity_reference_widgets_ha",
 *   module = "entity_reference_widgets",
 *   label = @Translation("Hierarchical Autocomplete"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class HierarchicalAutocomplete extends EntityReferenceAutocompleteWidget {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Only support taxonomy at the moment.
    if ($this->getFieldSetting('target_type') !== 'taxonomy_term') {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }

    $entity = $items->getEntity();
    $referenced_entities = $items->referencedEntities();

    // An id to wrap for ajax.
    $field_id = $this->fieldDefinition->getName() . '-' . $delta;

    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = $this->entityManager->getStorage($this->getFieldSetting('target_type'));

    // Load all top-level terms of selected vocab.
    $handlerSettings = $this->getFieldSetting('handler_settings');

    // @todo support multiple.
    $bundle = !empty($handlerSettings['target_bundles']) ? reset($handlerSettings['target_bundles']) : '';
    $terms = $storage->loadTree($bundle, 0, 1, TRUE);

    // @todo Fix this to be a config settings.
    $options = ['' => '--- Select Category ---'];
    foreach ($terms as $term) {
      $options[$term->id()] = $term->getName();
    }

    // Check the current term to get the current parent.
    if (isset($referenced_entities[$delta])) {
      /** @var \Drupal\taxonomy\TermInterface $current */
      $current = $referenced_entities[$delta];
      if ($parent = $current->parent->entity) {
        $pid = $parent->id();
      }
    }

    // Show a select of categories.
    $build['category'] = [
      '#type' => 'select',
      // @todo make this dynamic.
      '#title' => $this->t('Category'),
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'entityArgumentUpdate'],
        'wrapper' => $field_id,
      ],
      '#default_value' => isset($pid) ? $pid : '',
    ];

    // Append the match operation to the selection settings.
    $selection_settings = $this->getFieldSetting('handler_settings') + [
      'match_operator' => $this->getSetting('match_operator'),
      'match_limit' => $this->getSetting('match_limit'),
      'parent_selection' => $form_state->getValue([$this->fieldDefinition->getName(), $delta, 'category']),
    ];

    $element += [
      '#type' => 'entity_autocomplete',
      '#target_type' => $this->getFieldSetting('target_type'),
      '#selection_handler' => 'erw:hierarchical',
      '#selection_settings' => $selection_settings,
      // Entity reference field items are handling validation themselves via
      // the 'ValidReference' constraint.
      '#validate_reference' => FALSE,
      '#maxlength' => 1024,
      '#default_value' => isset($referenced_entities[$delta]) ? $referenced_entities[$delta] : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#prefix' => '<div id="' . $field_id . '">',
      '#suffix' => '</div>',
    ];

    if ($this->getSelectionHandlerSetting('auto_create') && ($bundle = $this->getAutocreateBundle())) {
      $element['#autocreate'] = [
        'bundle' => $bundle,
        'uid' => ($entity instanceof EntityOwnerInterface) ? $entity->getOwnerId() : \Drupal::currentUser()
          ->id(),
      ];
    }

    $build['target_id'] = $element;

    return $build;
  }

  /**
   * Ajax callback for selecting a category.
   *
   * @param array $form
   *   The Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The FormState.
   *
   * @return array
   *   The target element.
   */
  public function entityArgumentUpdate(array $form, FormStateInterface $formState) {
    $trigger = $formState->getTriggeringElement();
    $element = NestedArray::getValue($form, array_slice($trigger['#array_parents'], 0, -1));
    $formState->setRebuild(TRUE);
    return $element['target_id'];
  }

}
