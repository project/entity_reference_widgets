<?php

namespace Drupal\entity_reference_widgets\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Provides a hierarchical entity query select for taxonomy.
 *
 * @EntityReferenceSelection(
 *   id = "erw:hierarchical",
 *   label = @Translation("Taxonomy Term selection - Hierarchical"),
 *   entity_types = {"taxonomy_term"},
 *   group = "erw",
 *   weight = 1
 * )
 */
class Hierarchical extends DefaultSelection {

  /**
   * {@inheritDoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $configuration = $this->getConfiguration();
    if (empty($configuration['parent_selection'])) {
      return $query;
    }

    $query->condition('parent.target_id', $configuration['parent_selection']);
    return $query;
  }

}
